// console.log("Hello ALz");

// DISCUSSION #1

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/*txtFirstName.addEventListener("keyup", (event) =>{
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener("keyup", (event) =>{
	console.log(event.target);
	console.log(event.target.value);
});*/


// DISCUSSION #2

const updateFullName = ()=>{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`;
};
txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);